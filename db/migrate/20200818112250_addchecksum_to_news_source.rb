class AddchecksumToNewsSource < ActiveRecord::Migration[5.2]
  def change
    add_column :news_sources, :checksum, :string
  end
end
