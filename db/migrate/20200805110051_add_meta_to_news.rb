class AddMetaToNews < ActiveRecord::Migration[5.2]
  def change
    add_column :news, :meta, :jsonb, null: false, default: '{}'
    add_index :news, :meta, using: :gin
  end
end
