class CreateNewsSources < ActiveRecord::Migration[5.2]
  def change
    create_table :news_sources do |t|
      t.string :feed, null: false
      t.string :status, null: false
      t.belongs_to :news_category
      t.timestamps
    end
  end
end
