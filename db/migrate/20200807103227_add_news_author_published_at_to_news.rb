class AddNewsAuthorPublishedAtToNews < ActiveRecord::Migration[5.2]
  def change
    add_column :news, :author, :string
    add_column :news, :guid, :string
  end
end
