require_relative 'base_parser'
require_relative 'news_item'
require 'nokogiri'
module NewsTab
  class NewsMlParser < NewsTab::BaseParser

    def parse(content)
      content = Nokogiri::XML(content, nil , 'utf-8');
      content.xpath("//NewsItem").each do |item|
        news_item = NewsItem.new
        status = item.at_xpath(".//NewsManagement//Status")
        next if (status.attributes["FormalName"].value != "Usable")
        heading = item.xpath(".//NewsComponent//NewsLines//HeadLine").text
        news_item.set_news_title(heading)
        summary = item.xpath(".//NewsComponent//DataContent").first.text.squish
        news_item.set_news_description(summary)
        content = item.xpath(".//NewsComponent//DataContent").last.text.squish
        news_item.set_content(content)
        subject = item.at_xpath(".//SubjectCode//Subject").attributes
        categories = subject["FormalName"].value.split(",")
        news_item.set_news_categories(categories)
        url = item.at_xpath(".//Identification//NewsIdentifier//PublicIdentifier").child.text
        news_item.set_source_url(url)
        news_item.set_guid(url)
        self.add_news(news_item)
      end
    end
  end
end
