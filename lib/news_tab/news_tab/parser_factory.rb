require_relative 'rss_parser'
require_relative 'news_ml_parser'
require_relative 'rss_parser'

module NewsTab
  class ParserFactory


    def self.config
      {
          newsml: NewsMlParser,
          rss: RssParser,
      }
    end

    def self.configured_sources
      self.config.keys.map(&:to_s)
    end

    def self.parser(format)
      self.config[format.to_sym].new
    end
  end
end
