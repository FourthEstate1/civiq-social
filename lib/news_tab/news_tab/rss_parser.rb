require 'rss'
require_relative 'news_item'
require_relative 'base_parser'

module NewsTab
  class RssParser < NewsTab::BaseParser

    def parse(content)
      feeds = RSS::Parser.parse(content)
      feeds.items.each do |item|
        news_item = NewsItem.new
        news_item.set_news_title(item.title || item.description)
        news_item.set_news_description(item.description)
        news_item.set_guid(item.guid.content)
        news_item.set_source_url(item.link || item.channel.link)
        categories = []
        item.categories.each do |cat|
          categories << cat.content.downcase
        end
        news_item.set_news_categories(categories)
        news_item.set_media(item.enclosure.url) if item.enclosure.present?
        self.add_news(news_item)
      end
    end
  end
end
