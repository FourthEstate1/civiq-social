<div class="center">
<p align="center"><img src="app/javascript/images/civiq-logo.svg" align="center" width="30%"/></p>
<div class="center">







## Introduction

[Civiq](https://civiq.social) is a free, open, decentralized, and federated social network platform with an emphasis on protecting user privacy and avoiding the filter bubbles that are the result of intrusive AI algorithms.

It is a project from the [Fourth Estate](https://www.FourthEstate.org).

Civiq has unique features that empower and benefit local news organizations, and strengthens their connections to their communities and constituents.

Lastly, we will strive to make Civiq a platform where real people from around the world are free to responsibly express themselves. While actively fighting misinformation, disinformation, and mal-information.





##  Questions and issues

The [issue tracker](https://gitlab.com/FourthEstate1/civiq-social/issues) is only for bug reports and feature requests.
Anything else, such as questions or general feedback, should be posted in the [Civiq Group](ttps://civiq.social/groups/7).

For custom developent quotes, or enterprise support requests [email us](https://www.fourthestate.org/contact/).

## A special thanks to...

We would like to extend our thanks to the following investors, sponsors and supporters for funding Civiq's development. If you are interested in becoming a sponsor, please contact us at: [Contact Page](https://www.fourthestate.org/contact/)

- [<img src="https://www.fourthestate.org/media/branding/FE_logo_horizontal_2color.svg" width="250px">](https://www.fourthestate.org/)









## License

Copyright (C) 2016-2020 Eugen Rochko, Gab AI, Fourth Estate Public Benefit Corporation, and other contributors.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.

