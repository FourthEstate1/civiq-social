require 'rails_helper'

describe NewsTab::NewsMlParser do
  context 'parse' do
    describe do
      let(:content) { File.open(Rails.root + "spec/fixtures/files/newsml.xml") }

      before do
        subject.parse(content)
      end

      context 'News items' do
        let(:news) { subject.news }

        it 'should have five news ' do
          expect(news.count).to be 5
        end

        it 'should have valid headings' do
          headings = File.open(Rails.root + "spec/fixtures/files/news_heading.txt").map(&:strip);
          actual_headings = news.map(&:news_title);
          expect actual_headings.should == headings
        end

        it 'should match the summary' do
          expected = File.open(Rails.root + "spec/fixtures/files/news-summary.txt").map(&:squish);
          actual = news.map(&:news_description);
          expect actual.should == expected
        end

        it 'should match all the contents' do
          content = news.map(&:content)
          expect(content.size).to eq 5
        end

        it 'should have proper categories' do
          expected = [['health'], ['politics'], ['government'], ['politics'], ['politics']]
          actual = news.map(&:news_categories);
          expect actual.should == expected
        end
      end
    end
  end
end
