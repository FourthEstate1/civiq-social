require 'rails_helper'

describe 'NewsSource' do

  describe '#before save' do

    context 'when format is valid' do

      it 'should not save' do
        news_source = Fabricate.build(:news_source, format: 'invalid');
        expect(news_source.save).to be false
      end
    end
  end
end
