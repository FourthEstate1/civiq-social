require 'rails_helper'

describe NewsSourceFilter do

  let(:politics) { Fabricate(:news_category, name: 'politics') }
  let(:sports) { Fabricate(:news_category, name: 'sports') }

  context 'with empty params' do
    it 'fetches all all sources' do
      feed1 = Fabricate(:news_source, news_category: sports)
      (1..5).each do |n|
        Fabricate(:news_source, news_category: politics)
      end
      filter = described_class.new({})
      expect(filter.results.all.count).to eq NewsSource.all.count
    end
  end

  context 'when by category' do
    it 'fetches all all sources' do
      sport_category = Fabricate(:news_source, news_category: sports)
      politics_feed = Fabricate(:news_source, news_category: politics)
      filter = described_class.new({'category': sports.id})
      filter.results.each do |result|
        expect(result.news_category).to eq result.news_category
      end
    end
  end

  context 'when by active' do
    it 'fetch all active status' do
      Fabricate(:news_source, status: 'inactive', news_category: sports)
      Fabricate(:news_source, status: 'active', news_category: politics)
      filter = described_class.new({'status': 'active'})
      results = filter.results
      expect(results.size).to eq 1
      filter.results.each do |result|
        expect(result.status).to eq 'active'
      end
    end
  end

  context 'when by feed' do
    it 'fetch all matching' do
      Fabricate(:news_source, status: 'active', news_category: sports, feed: 'https://www.wired.com/feed/category/business/latest/rss')
      Fabricate(:news_source, status: 'active', news_category: sports, feed: 'https://www.newsminute.com/feed/category/business/latest/rss')
      Fabricate(:news_source, status: 'active', news_category: sports, feed: 'https://www.newsminute.com/feed/category/politics/latest/rss')
      filter = described_class.new({'feed': 'https://www.newsminute.com'})
      results = filter.results.all.count
      expect(results).to eq 2
    end
  end
end
