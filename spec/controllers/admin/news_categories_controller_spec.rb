require 'rails_helper'

describe Admin::NewsCategoriesController, type: :controller do

  render_views
  let(:user) { Fabricate(:user, admin: true) }

  before do
    sign_in user, scope: :user
  end

  describe 'GET #index' do
    subject { get :index }

    let!(:news_category) { Fabricate(:news_category) }
    it 'renders index page' do
      print(response)
      expect(subject).to render_template :index
    end
  end

  describe 'POST #create' do

  end

  describe 'POST #sort' do

    let!(:news_category) { Fabricate(:news_category, name: 'title', position: 1) }
    let!(:sports_category) { Fabricate(:news_category, name: 'sports', position: 2) }

    it 'updates the sort order of categories' do
      routes.draw { post 'sort' => 'admin/news_categories#sort' }
      post :sort, params: {news_category: [news_category.id, sports_category.id]}
      expect(news_category.reload.position).to be 1
      expect(sports_category.reload.position).to be 2
    end
  end
end
