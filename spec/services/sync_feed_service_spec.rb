require 'rails_helper'

describe SyncFeedService do

  let(:news_source) { Fabricate(:news_source, etag: "sdsds", format: 'rss') }
  context '#call' do
    context 'when new source is updated' do
      before do
        response = stub_request(:get, "https://www.wired.com/feed/category/science/latest/rss").
            with(
                headers: {
                    'Accept' => '*/*',
                    'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                    'If-Match' => news_source.etag,
                    'User-Agent' => 'Ruby'
                }).
            to_return(status: 200, body: "Sample", headers: {"etag": "etag"})
      end

      it 'expect to call populate service' do

        service = double("service")
        expect(service).to receive(:call)

        subject.call(news_source, service)

        expected_news_source = NewsSource.find_by_id news_source.id
        expect(expected_news_source.etag).to eq "etag"
      end

      it 'should update all new_source' do
        service = double("service")
        expect(service).to receive(:call)

        subject.call(news_source, service)

        expected_news_source = NewsSource.find_by_id news_source.id
        expect(expected_news_source.etag).to eq "etag"
        expect(expected_news_source.checksum).to eq "Sample".hash.to_s
      end
    end

    context 'when server content has not modified' do
      before do
        stub_request(:get, "https://www.wired.com/feed/category/science/latest/rss").
            with(
                headers: {
                    'Accept' => '*/*',
                    'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3',
                    'If-Match' => news_source.etag,
                    'User-Agent' => 'Ruby'
                }).
            to_return(status: 304, body: "", headers: {"etag": "etag"})
      end

      it 'should not call populate news service' do
        service = double("service")
        subject.call(news_source, service)
        expect(subject).not_to receive(:call)
      end

    end

    context 'when response has not changed' do
      it 'should not call populate news service' do
        service = double("service")
        news_source = Fabricate(:news_source, etag: "sdsds", format: 'rss', checksum: 'Sample'.hash)
        expect(subject).not_to receive(:call)
      end
    end
  end

end
