require 'rss'

class PopulateNewsService < BaseService

  def call(content, format, news_category)
    parser = NewsTab::ParserFactory.parser(format)
    parser.parse(content)
    news = parser.news
    old_news = News.where(:guid => news.map(&:guid)).pluck(:guid)
    new_items = news.delete_if {|item| item.guid.in? old_news }
    new_items.each do |entry|
      news = News.new
      news.news_category = news_category
      news.source_name = entry.news_title
      news.description = entry.news_description
      news.content = entry.content
      news.source_url = entry.source_url
      news.image = entry.media
      news.guid = entry.guid
      news.meta = {categories: []}
      news.meta["categories"] = []
      news.meta["categories"] = (entry.news_categories.map(&:downcase) + [news_category.name]+  [NewsCategory.common_category]).uniq
      news.title = entry.news_title
      news.save!
    end
  end
end
