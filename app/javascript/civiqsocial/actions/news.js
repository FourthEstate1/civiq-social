import api from '../api';

export const CATEGORY_FETCH_SUCCESS = 'CATEGORY_FETCH_SUCCESS';
export const CATEGORY_FETCH_FAILURE = 'CATEGORY_FETCH_FAILURE';
export const CATEGORY_FETCH_REQUEST = 'CATEGORY_FETCH_REQUEST';

export const FETCH_NEWS_REQUEST = 'FETCH_NEWS_REQUEST';
export const FETCH_NEWS_SUCCESS = 'FETCH_NEWS_SUCCESS';
export const FETCH_NEWS_FAILURE = 'FETCH_NEWS_FAILURE';

export const EXPAND_NEWS_REQUEST = 'EXPAND_NEWS_REQUEST';
export const EXPAND_NEWS_SUCCESS = 'EXPAND_NEWS_SUCCESS';
export const EXPAND_NEWS_FAILURE = 'EXPAND_NEWS_FAILURE';

export const FETCH_NEWS_DETAILS_REQUEST = 'FETCH_NEWS_DETAILS_REQUEST';
export const FETCH_NEWS_DETAILS_SUCCESS = 'FETCH_NEWS_DETAILS_SUCCESS';
export const FETCH_NEWS_DETAILS_FAILURE = 'FETCH_NEWS_DETAILS_FAILURE';


export const fetchCategoriesRequest = id => ({
  type: CATEGORY_FETCH_REQUEST,
  id,
});

export const fetchCategoriesRequestSuccess = data => ({
  type: CATEGORY_FETCH_SUCCESS,
  news_categories: data,
});
export const fetchCategoriesFail = (error) => ({
  type: CATEGORY_FETCH_FAILURE,
  error: error,
});

export const fetchCategories = () => (dispatch, getState) => {
  dispatch(fetchCategoriesRequest());
  api(getState).get('/api/v1/news_categories')
    .then(({ data }) => {
      dispatch(fetchCategoriesRequestSuccess(data));
    })
    .catch(err => dispatch(fetchCategoriesFail(err)));
};

export const fetchNewsRequest = (id, page = 1) => ({
  type: FETCH_NEWS_REQUEST,
  id,
  page,
});

export const fetchNewsRequestSuccess = data => ({
  type: FETCH_NEWS_SUCCESS,
  news: data.news,
  pagination: data.meta,
});
export const fetchNewsRequestFailure = (id, error) => ({
  type: FETCH_NEWS_FAILURE,
  error: error,
  id,
});

export const expandFetchNewsRequest = (category, next) => ({
    type: EXPAND_NEWS_REQUEST,
    category,
    next,
  }
);

export const expandFetchNewsSuccess = (data) => ({
    type: EXPAND_NEWS_SUCCESS,
    news: data.news,
    pagination: data.meta,
  }
);

export const expandFetchNewsFailure = (id, error) => ({
  type: EXPAND_NEWS_FAILURE,
  error: error,
  id,
});


export const fetchNews = (category, page = 1) => (dispatch, getState) => {
  dispatch(fetchNewsRequest(category, page));
  api(getState).get('/api/v1/timelines/news?page=' + page + '&news_category=' + category)
    .then(({ data }) => {
      dispatch(fetchNewsRequestSuccess(data));
    })
    .catch(err => dispatch(fetchNewsRequestFailure(category, err)));
};

export const expandFetchNews = (category) => (dispatch, getState) => {
  const meta = getState().getIn(['news', 'news_meta']);
  const next = meta.next_page;
  if (!next) {
    return;
  }
  dispatch(expandFetchNewsRequest(category, next));
  api(getState).get('/api/v1/timelines/news?page=' + next + '&news_category=' + category)
    .then(({ data }) => {
      dispatch(expandFetchNewsSuccess(data));
    })
    .catch(err => {
      dispatch(expandFetchNewsFailure(category, err));
    });
};

export const fetchNewsDetailsRequest = id => ({
  type: FETCH_NEWS_DETAILS_REQUEST,
  id,
});

export const fetchNewsDetailsSuccess = (data) => ({
    type: FETCH_NEWS_DETAILS_SUCCESS,
    data,
  }
);

export const fetchNewsDetailsFailure = (id, error) => ({
    type: FETCH_NEWS_DETAILS_SUCCESS,
    error: error,
    id,
  }
);

export const fetchNewsDetails = (id) => (dispatch, getState) => {
  dispatch(fetchNewsDetailsRequest(id));
  api(getState).get('/api/v1/timelines/news/' + id)
    .then(({ data }) => {
      dispatch(fetchNewsDetailsSuccess(data));
    })
    .catch(err => {
      dispatch(fetchNewsDetailsFailure(id, err));
    });
};
