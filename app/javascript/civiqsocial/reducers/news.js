import {
  CATEGORY_FETCH_FAILURE,
  CATEGORY_FETCH_SUCCESS,
  EXPAND_NEWS_SUCCESS,
  FETCH_NEWS_REQUEST,
  FETCH_NEWS_SUCCESS,
  FETCH_NEWS_DETAILS_SUCCESS,
  FETCH_NEWS_DETAILS_FAILURE,
  FETCH_NEWS_DETAILS_REQUEST
} from '../actions/news';
import { Map as ImmutableMap } from 'immutable';

const initialState = ImmutableMap();


export default function news(state = initialState, action) {
  switch (action.type) {
  case CATEGORY_FETCH_SUCCESS: {
    return state.set('news_categories', action.news_categories);
  }
  case CATEGORY_FETCH_FAILURE:
    return state.set(action.id, false);

  case FETCH_NEWS_REQUEST: {
    state = state.delete('news_meta');
    state = state.delete('news');
    return state;
  }


  case FETCH_NEWS_SUCCESS: {
    state = state.set('news_meta', action.pagination);
    state = state.set('news', action.news);
    return state;
  }

  case EXPAND_NEWS_SUCCESS: {
    state = state.set('news_meta', action.pagination);
    const news = state.get('news', []);
    state = state.set('news', news.concat(action.news));
    return state;
  }
  case FETCH_NEWS_DETAILS_REQUEST: {
    state = state.delete('news_item_detail');
    return state;
  }
  case FETCH_NEWS_DETAILS_SUCCESS: {
    state = state.set('news_item_detail', action.data);
    return state;
  }

  default:
    return state;
  }
}


