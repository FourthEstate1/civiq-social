import React from 'react';
import { connect } from 'react-redux';
import { defineMessages, FormattedMessage, injectIntl } from 'react-intl';
import ImmutablePureComponent from 'react-immutable-pure-component';
import { Redirect } from 'react-router-dom';
import { fetchNewsDetails } from '../../../actions/news';
import NewsCard from '../card';
import Column from '../../ui/components/column';
import LoadingIndicator from '../../../components/loading_indicator';
import ColumnHeader from '../../ui/components/column_header';


const messages = defineMessages({
  heading: { id: 'column.news', defaultMessage: 'News' },
});


const mapStateToProps = (state, { params: { category, newsID } }) => {
  return {
    categories: state.getIn(['news', 'news_categories']),
    categoryName: category,
    newsID,
    newsItemDetail: state.getIn(['news', 'news_item_detail']),
  };
};


export default @connect(mapStateToProps)
@injectIntl
class NewsDetail extends ImmutablePureComponent {

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (prevProps.categoryName !== this.props.categoryName) {
      // this.props.dispatch(fetchNews(this.props.categoryName));
    }
  }

  componentDidMount() {
    this.props.dispatch(fetchNewsDetails(this.props.newsID));
  }




  render() {
    const { intl, newsItemDetail } = this.props;
    if(!newsItemDetail) {
      return (
        <div style={{ marginTop: '10px' }}>
           <LoadingIndicator />
        </div>
      );
    }
    const publishDate = new Date(1000 * newsItemDetail.published_at).toLocaleString();
    const categories = newsItemDetail.categories.join(', ');
    return (
      <div className='news-detail'>
        <Column>
          <div className='news-detail__item'>
            <h1 className='news-detail__header'>
              {newsItemDetail.title}
            </h1>
            <div className='news-detail__meta'>
              <strong>{categories}</strong>
              <div className='news-detail__published_at'>Published on: {publishDate}</div>
            </div>
            <div className='news-detail__image'>{newsItemDetail.image && <img alt="news image" src={newsItemDetail.image}/>}</div>
            <div className='news-detail__content' dangerouslySetInnerHTML={{ __html: newsItemDetail.content }} />
          </div>

        </Column>


      </div>
    );
  };

}
