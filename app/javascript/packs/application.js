import loadPolyfills from '../civiqsocial/load_polyfills';

loadPolyfills().then(() => {
  require('../civiqsocial/main').default();
}).catch(e => {
  console.error(e);
});

