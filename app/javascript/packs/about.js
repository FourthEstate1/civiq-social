'use strict';

import loadPolyfills from '../civiqsocial/load_polyfills';
import { start } from '../civiqsocial/common';

start();

function loaded() {
  const TimelineContainer = require('../civiqsocial/containers/timeline_container').default;
  const React             = require('react');
  const ReactDOM          = require('react-dom');
  const mountNode         = document.getElementById('civiqsocial-timeline');

  if (mountNode !== null) {
    const props = JSON.parse(mountNode.getAttribute('data-props'));
    ReactDOM.render(<TimelineContainer {...props} />, mountNode);
  }
}

function main() {
  const ready = require('../civiqsocial/ready').default;
  ready(loaded);
}

loadPolyfills().then(main).catch(error => {
  console.error(error);
});
