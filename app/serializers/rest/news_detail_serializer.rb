class REST::NewsDetailSerializer < ActiveModel::Serializer
  attributes :title, :source_name
  attributes :categories
  attributes :image
  attributes :content
  attributes :published_at

  def content
    CGI::unescapeHTML(object.content)
  end

  def published_at
    object.created_at.to_i
  end

  def categories
   object.categories.delete(NewsCategory.common_category)
   object.categories
  end
end

