class REST::NewsCategoriesSerializer <ActiveModel::Serializer
  attributes :name
  attributes :id

  def name
    object.name.titleize
  end
end
