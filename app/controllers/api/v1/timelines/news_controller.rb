# frozen_string_literal: true
class Api::V1::Timelines::NewsController < Api::BaseController
  include RoutingHelper

  respond_to :json
  delegate :url_helpers, to: 'Rails.application.routes'
  DEFAULT_PAGE_SIZE = 10

  before_action -> { doorkeeper_authorize! :read, :'read:news' }
  before_action :require_user!

  def index
    news = News.where("meta->'categories'? :category",
                      category: params[:news_category].downcase
    ).order('created_at desc').page(page_number).per(page_size)
    if news.nil?
      render_empty
      return
    end
    render json: REST::PaginationSerializer.build(news, REST::NewsSerializer)
  end

  def show
    news = News.find_by_id params[:id]
    render json: news, serializer: REST::NewsDetailSerializer
  end

  private

  def pagination_dict(collection)
    {
        current_page: collection.current_page,
        next_page: collection.next_page,
        prev_page: collection.prev_page,
        total_pages: collection.total_pages,
        total_count: collection.total_count,
    }
  end

  def page_number
    params[:page].present? && params[:page] ? params[:page] : 1
  end

  def page_size
    DEFAULT_PAGE_SIZE
  end
end
