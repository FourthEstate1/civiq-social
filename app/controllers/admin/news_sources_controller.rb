# frozen_string_literal: true
module Admin
  class NewsSourcesController < BaseController
    def index
      authorize :news_source, :index?
      @news_sources = filtered_news_sources.page(params[:page])
    end

    def new
      authorize :news_source, :new?
      @categories = NewsCategory.order('name desc').all
      @news_source = NewsSource.new
      @formats = NewsSource.allowed_formats
      @statuses = %w(active inactive)
    end

    def create
      authorize :news_source, :create?
      @news_source = NewsSource.new(resource_params)
      if @news_source.save
        redirect_to admin_news_sources_path("category": @news_source.news_category.id)
      else
        @news_sources = NewsSource.page(params[:page])
        render :index
      end
    end

    def activate
      authorize :news_source, :activate?
      news_source = NewsSource.find_by_id params[:news_source_id]
      news_source.activate
      news_source.save!
      redirect_to admin_news_sources_path
    end

    def deactivate
      authorize :news_source, :deactivate?
      news_source = NewsSource.find_by_id params[:news_source_id]
      news_source.deactivate
      news_source.save
      redirect_to admin_news_sources_path
    end

    def destroy
      authorize :news_source, :destroy?
      news = NewsSource.find_by_id params[:id]
      news.destroy!
      redirect_to admin_news_sources_path
    end

    def filtered_news_sources
      NewsSourceFilter.new(filter_params).results
    end

    def filter_params
      params.permit(:news_category_id, :status, :feed)
    end

    def resource_params
      params.require(:news_source).permit(:feed, :news_category_id, :status, :format)
    end
  end
end
