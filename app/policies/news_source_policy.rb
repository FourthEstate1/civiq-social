# frozen_string_literal: true

class NewsSourcePolicy < ApplicationPolicy

  def index?
    admin?
  end

  def create?
    admin?
  end

  def destroy?
    admin?
  end

  def new?
    admin?
  end

  def activate?
    admin?
  end

  def deactivate?
    admin?
  end

end

