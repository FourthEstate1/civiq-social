# == Schema Information
#
# Table name: news_categories
#
#  id         :bigint(8)        not null, primary key
#  name       :string           not null
#  position   :integer
#  created_at :datetime         not null
#  updated_at :datetime         not null
#

class NewsCategory < ApplicationRecord

  validates :name, uniqueness: true, presence: true

  has_many :news, dependent: :destroy
  has_many :news_source, dependent: :destroy

  before_validation :normalize_name, on: :create

  private

  def normalize_name
    self.name = name.downcase
  end

  def self.common_category
    'all'
  end
end
