# == Schema Information
#
# Table name: news_sources
#
#  id               :bigint(8)        not null, primary key
#  feed             :string           not null
#  status           :string           not null
#  news_category_id :bigint(8)
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#  etag             :string
#  last_modified_at :datetime
#  format           :string
#  checksum         :string
#

class NewsSource < ApplicationRecord

  include NewsTab
  belongs_to :news_category

  validates :feed, format: {with: URI.regexp}, presence: true

  validates :status, inclusion: {in: %w(active inactive)}

  before_save :default_value

  validates :format, inclusion: {in: NewsTab::ParserFactory.configured_sources}

  def self.allowed_formats
    NewsTab::ParserFactory.configured_sources
  end

  def default_value
    self.feed ||= 'active'
  end

  def self.filter_by_feed(feed)
    pattern = self.sanitize_sql_like(feed.strip) + '%'
    self.where('lower(feed) like lower(?)', pattern)
  end

  def self.filter_by_status(status)
    NewsSource.where(status: status)
  end

  def self.active_sources
    self.filter_by_status('active')
  end

  def categoryName
    self.category.name
  end

  def activate
    self.status = 'active'
  end

  def deactivate
    self.status = 'inactive'
  end

  def active?
    self.status == 'active'
  end

  def modified?(content)
    content.hash != checksum
  end
end
